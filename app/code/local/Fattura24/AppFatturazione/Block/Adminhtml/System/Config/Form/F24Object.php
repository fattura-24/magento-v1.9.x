<?php

class Fattura24_AppFatturazione_Block_Adminhtml_System_Config_Form_F24Object
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
       
        $html = "<button id='f24_default_object' onclick='F24Object()'>";
        $html .= "Predefinito";
        $html .="</button>";
        
    
    return $html;

    }    
}
?>
<script>
function F24Object() {
    document.getElementById('fattura24_fatture_causale_doc').defaultValue = "Ordine Magento (N)";
}
</script>
<?