<?php

class Fattura24_AppFatturazione_Block_Adminhtml_System_Config_Form_InfoLink
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
       
        $html = "<a style='font-size:normal;' href='https://www.fattura24.com/magento-modulo-fatturazione/' target='_blank'>";
        $html .= "Supporto";
        $html .= "</a><br />";
        
        $html .= "<a style='font-size:normal;' href='https://www.fattura24.com/termini-utilizzo/' target='_blank'>";
        $html .= "Condizioni di contratto e termini di utilizzo di Fattura24";
        $html .= "</a> <br />";
        
        $html .= "<a style='font-size:normal;' href='https://www.fattura24.com/regolamento-ecommerce/' target='_blank'>";
        $html .= "Regolamento F24 integrativo (delle Condizioni di Contratto) per modulo Magento";
        $html .= "</a> <br />";

        $html .= "<a href='https://www.fattura24.com/policy/' target='_blank'>";
        $html .= "Privacy di Fattura24";
        $html .= "</a> <br />";
    
    return $html;
    }    
}