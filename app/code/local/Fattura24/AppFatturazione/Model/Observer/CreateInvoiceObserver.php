<?php

class Fattura24_AppFatturazione_Model_Observer_CreateInvoiceObserver
{
    public function createInvoice(Varien_Event_Observer $observer)
    {
        $order = $observer->getEvent()->getInvoice()->getOrder();  
        //if((Mage::helper('appfatturazione')->getConfig('fattura24/fatture/crea_fattura', $order->getStoreId()) == 'f' || Mage::helper('appfatturazione')->getConfig('fattura24/fatture/crea_fattura', $order->getStoreId()) == 'fe') && !Mage::helper('appfatturazione')->getDocIdInvoice($order))
        $select_crea_fattura = Mage::helper('appfatturazione')->getConfig('fattura24/fatture/crea_fattura', $order->getStoreId());
        if($select_crea_fattura == 'fm' && !Mage::helper('appfatturazione')->getDocIdInvoice($order))
           Mage::helper('appfatturazione')->saveDocument($order, 'I');
        elseif($select_crea_fattura == 'fem' && !Mage::helper('appfatturazione')->getDocIdInvoice($order))
           Mage::helper('appfatturazione')->saveDocument($order, 'FE');   
    }
}