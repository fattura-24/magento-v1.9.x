<?php

class Fattura24_AppFatturazione_Model_System_Config_Source_SelectStatoPagato
{

    public function toOptionArray(){
        $option = array(
            array('value' => 0, 'label' => "Mai "),
            array('value' => 1, 'label' => "Sempre "),
            array('value' => 2, 'label' => "Pagamenti elettronici (es.: Paypal) ")
        );
        return $option;
    }
}
?>