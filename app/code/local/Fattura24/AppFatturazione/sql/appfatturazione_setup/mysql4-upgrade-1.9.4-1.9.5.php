<?php
$setup = $this;
$setup->startSetup();
$connection = $setup->getConnection();

$setup->addAttribute('customer_address', 'customer_recipientcode', array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => 'Customer Recipientcode',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'is_user_defined' => true,
    'visible_on_front' => 1
));
Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'customer_recipientcode')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address'))
    ->save();

$setup->addAttribute('customer_address', 'customer_pec', array(
    'type' => 'varchar',
    'input' => 'text',
    'label' => 'Customer Pec',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'is_user_defined' => true,
    'visible_on_front' => 1
));


Mage::getSingleton('eav/config')
    ->getAttribute('customer_address', 'customer_pec')
    ->setData('used_in_forms', array('customer_register_address','customer_address_edit','adminhtml_customer_address'))
    ->save();

$tablequote = $setup->getTable('sales/quote_address');
$query = "ALTER TABLE ".$tablequote." ADD 'customer_recipientcode' varchar(255) NOT NULL";
$setup->getConnection()->query($query);

$query1 = "ALTER TABLE ".$tablequote." ADD 'customer_pec' varchar(255) NOT NULL";
$setup->getConnection()->query($query);

$tablequote1 = $setup->getTable('sales/order_address');
$query2 = "ALTER TABLE ".$tablequote1." ADD 'customer_recipientcode' varchar(255) NOT NULL";
$setup->getConnection()->query($query2);

$query3 = "ALTER TABLE ".$tablequote1." ADD 'customer_pec' varchar(255) NOT NULL";
$setup->getConnection()->query($query3);

$setup->endSetup();